package test;
import java.io.IOException;
import movies.importer.LowercaseProcessor;
import movies.importer.RemoveDuplicates;
public class ProcessingTest {

	public static void main(String[] args) throws IOException {
		String sourceDir = "C:\\Users\\1533920\\Desktop\\inputTxtFiles";
		String outputDir = "C:\\Users\\1533920\\Desktop\\outputTxtFiles";
		String output2 = "C:\\Users\\1533920\\Desktop\\outputTxtFilesRemoveDuplicates";
		LowercaseProcessor proc = new LowercaseProcessor(sourceDir,outputDir);
		proc.execute();
		RemoveDuplicates rmDup = new RemoveDuplicates(outputDir, output2);
		rmDup.execute();
	}

}
