package movies.importer;
import java.util.*;
import java.io.*;
public class RemoveDuplicates extends Processor{
	public RemoveDuplicates(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> output = new ArrayList<String>();
		for (String s : input) {
			if(!output.contains(s)) {
				output.add(s);
			}
		}
		return output;
	}
}
