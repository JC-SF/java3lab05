package movies.importer;
import java.util.*;
public class LowercaseProcessor extends Processor{
	public LowercaseProcessor (String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> asLower = new ArrayList<String>();
		for (String s : input) {
			asLower.add(s.toLowerCase());
		}
		return asLower;
	}
	
}
